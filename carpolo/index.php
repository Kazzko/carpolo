<?php 
    // initialize errors variable
	$errors = "";

	// connect to database
	$db = mysqli_connect("localhost", "root", "", "todo");

	// insert a quote if submit button is clicked
	if (isset($_POST['submit'])) {
		if (empty($_POST['task'])) {
			$errors = "You must fill in the task";
		}else{
			$task = $_POST['task'];
			$sql = "INSERT INTO tasks (task) VALUES ('$task')";
			mysqli_query($db, $sql);
			header('location: index.php');
		}
    }	

    if (isset($_GET['del_task'])) {
        $id = $_GET['del_task'];
    
        mysqli_query($db, "DELETE FROM tasks WHERE id=".$id);
        header('location: index.php');
    }

    if (isset($_GET['done_task'])) {
        $id = $_GET['done_task'];
    
        mysqli_query($db, "UPDATE tasks SET done='1' WHERE id=".$id);
        header('location: index.php');
    }

    if (isset($_GET['undo_task'])) {
        $id = $_GET['undo_task'];
    
        mysqli_query($db, "UPDATE tasks SET done='0' WHERE id=".$id);
        header('location: index.php');
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="grid.css">
    <title>Todo List</title>
</head>
<body>
    <nav>
        <span class="heading_title">Todo  List</span>
    </nav>
    <div class="row">
        <div class="form_div">
        <form method="post" action="index.php">
            <input name="task" type="text" class="task_input" placeholder="Enter Task...">
            <button type="submit" name="submit" class="addTask_btn" id="addTask_btn--id">Add</button>
        </form>
        </div>
        <div class="tasks_view">
            <div class="col span-1-of-2 task_undone">
                <h2>Tasks</h2>


                <table>
                    <thead>
                        <tr>
                            <th></th>
                            <th class="first_head"></th>
                            <th></th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php 
                        // select all tasks if page is visited or refreshed
                        $tasks = mysqli_query($db, "SELECT * FROM tasks WHERE done = '0'");

                        $i = 1; while ($row = mysqli_fetch_array($tasks)) { ?>
                            <tr>
                                <td> <?php echo $i; ?> </td>
                                <td class="task"> <?php echo $row['task']; ?> </td>
                                <td class="done"> 
                                    <a href="index.php?done_task=<?php echo $row['id'] ?>">✔</a> 
                                </td>
                            </tr>
                        <?php $i++; } ?>	
                    </tbody>
                </table>



            </div>
            <div class="col span-1-of-2 task_done">
                <h2>Done</h2>

                <table>
                    <thead>
                        <tr>
                            <th></th>
                            <th class="first_head"></th>
                            <th></th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php

                        $tasks = mysqli_query($db, "SELECT * FROM tasks WHERE done = '1'");

                        $i = 1; while ($row = mysqli_fetch_array($tasks)) { ?>
                            <tr>
                                <td> <?php echo $i; ?> </td>
                                <td class="task"> <?php echo $row['task']; ?> </td>
                                <td class="undo"> 
                                    <a href="index.php?undo_task=<?php echo $row['id'] ?>">⟲</a> 
                                </td>
                                <td class="delete"> 
                                    <a href="index.php?del_task=<?php echo $row['id'] ?>">x</a> 
                                </td>
                            </tr>
                        <?php $i++; } ?>	
                    </tbody>
                </table>


            </div>
        </div>
    </div>

    <script src="404.js"></script> <!-- Adding this forces CSS transitions not to start on page load; Fixes CSS transition bug -->
</body>
</html>